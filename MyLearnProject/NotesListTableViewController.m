//
//  NotesListTableViewController.m
//  MyLearnProject
//
//  Created by Anton Petrov on 11/21/16.
//  Copyright © 2016 Anton Petrov. All rights reserved.

#import "NotesListTableViewController.h"
#import "NoteDetailViewController.h"

@interface NotesListTableViewController ()

@end

@implementation NotesListTableViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowNote"]) {
        NoteDetailViewController *noteDetailViewController = [segue destinationViewController];
        NSIndexPath *selectedIndexPath = self.tableView.indexPathForSelectedRow;
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray* arr = [defaults objectForKey:@"arrayOfTitles_A_V_Petrov_Test"];        
        noteDetailViewController.passedTitle = [arr objectAtIndex:selectedIndexPath.row];
        noteDetailViewController.passedText = [defaults objectForKey:noteDetailViewController.passedTitle];
    }
    else if ([segue.identifier isEqualToString:@"AddNote"]) {
        NoteDetailViewController *noteDetailViewController = [segue destinationViewController];
        noteDetailViewController.passedTitle = @"";
        noteDetailViewController.passedText = @"";
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"arrayOfTitles_A_V_Petrov_Test"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotesCell" forIndexPath:indexPath];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    cell.textLabel.text = [[defaults objectForKey:@"arrayOfTitles_A_V_Petrov_Test"]objectAtIndex:indexPath.row];
    return cell;
}
@end
