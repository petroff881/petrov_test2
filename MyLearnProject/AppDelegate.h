//
//  AppDelegate.h
//  MyLearnProject
//
//  Created by Anton Petrov on 11/20/16.
//  Copyright © 2016 Anton Petrov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

