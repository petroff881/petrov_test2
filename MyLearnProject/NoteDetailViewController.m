//
//  NoteDetailViewController.m
//  MyLearnProject
//
//  Created by Anton Petrov on 11/21/16.
//  Copyright © 2016 Anton Petrov. All rights reserved.
//

#import "NoteDetailViewController.h"

@interface NoteDetailViewController ()

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UITextView *titleTextField;

- (IBAction)save:(id)sender;

@end

@implementation NoteDetailViewController

@synthesize passedTitle;
@synthesize passedText;

- (IBAction)save:(id)sender {
    NSString *title = [_titleTextField text];
    NSString *note  = [_contentTextView text];
    if([title isEqualToString:@""] || [note isEqualToString:@""]) {
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"arrayOfTitles_A_V_Petrov_Test"] == nil) {
        NSMutableArray* titlesContainer = [[NSMutableArray alloc] init];
        [titlesContainer addObject:title];
        [defaults setObject:titlesContainer forKey:@"arrayOfTitles_A_V_Petrov_Test"];
        [defaults setObject:note forKey:title];
        [defaults synchronize];
    }
    else {
        NSMutableArray* titlesContainer = [[NSMutableArray alloc] initWithArray:[defaults objectForKey:@"arrayOfTitles_A_V_Petrov_Test"]];
        [defaults removeObjectForKey:@"arrayOfTitles_A_V_Petrov_Test"];
        [titlesContainer addObject:title];
        [defaults setObject:titlesContainer forKey:@"arrayOfTitles_A_V_Petrov_Test"];
        [defaults setObject:note forKey:title];
        [defaults synchronize];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.titleTextField.text = self.passedTitle;    
    self.contentTextView.text = self.passedText;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
